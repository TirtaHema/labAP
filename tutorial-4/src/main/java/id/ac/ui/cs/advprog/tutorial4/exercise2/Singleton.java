package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    private static Singleton unique;

    private Singleton() { }

    public static Singleton getInstance() {
        if (unique == null) {
            unique = new Singleton();
        }
        return unique;
    }
}
