package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BigClams implements Clams {
    public String toString() {
        return "Big Clams";
    }
}
