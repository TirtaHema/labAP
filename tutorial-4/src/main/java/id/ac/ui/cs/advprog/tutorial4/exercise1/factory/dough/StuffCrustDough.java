package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class StuffCrustDough implements Dough {
    public String toString() {
        return "Stuff Crust Dough";
    }
}
