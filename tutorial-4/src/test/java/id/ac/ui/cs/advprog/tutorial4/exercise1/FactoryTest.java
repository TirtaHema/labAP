package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class FactoryTest {

    protected PizzaStore pizzaStoreNy;
    protected PizzaStore pizzaStoreDpk;

    @Before
    public void setUp() {
        pizzaStoreNy = new NewYorkPizzaStore();
        pizzaStoreDpk = new DepokPizzaStore();
    }

    @Test
    public void orderPizza() {
        Pizza pizza;

        //NY Store
        pizza = pizzaStoreNy.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza",pizza.getName());

        pizza = pizzaStoreNy.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza",pizza.getName());

        pizza = pizzaStoreNy.orderPizza("clam");
        assertEquals("New York Style Clam Pizza",pizza.getName());

        //DPK Store
        pizza = pizzaStoreDpk.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza",pizza.getName());

        pizza = pizzaStoreDpk.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza",pizza.getName());


    }


}
