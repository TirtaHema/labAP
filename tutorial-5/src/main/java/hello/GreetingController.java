package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                   String name, Model model) {
        /* model.addAttribute("name", name); */
        if (name == null || name.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", name + ", I hope you interested");
        }

        StringBuilder cv = new StringBuilder();
        cv.append("Tirta Hema\n");
        cv.append("Birthdate: 2 Juli 1998\n");
        cv.append("Birthplace: Jakarta\n");
        cv.append("Address: kelapa gading");
        cv.append("Education history:\n");
        cv.append(" - Universitas Indonesia (2016-now)\n");
        cv.append(" - SMAK 5 (2013-2016)\n");
        cv.append(" - SMPK 4 (2010-2013)\n");
        model.addAttribute("cv", cv.toString());

        StringBuilder desc = new StringBuilder();
        desc.append("I'm undergraduate student at Universitas Indonesia. \n");
        model.addAttribute("description", desc.toString());


        return "greeting";
    }

}
