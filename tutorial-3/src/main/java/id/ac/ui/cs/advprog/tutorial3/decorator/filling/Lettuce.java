package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Food {
    Food food;

    public Lettuce(Food food) {
        this.food = food; //TODO Implement
    }

    @Override
    public String getDescription() {
        return this.food.getDescription()+", adding lettuce";//TODO Implement
    }

    @Override
    public double cost() {
        return this.food.cost()+0.75;//TODO Implement
    }
}
