package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        this.food = food; //TODO Implement
    }

    @Override
    public String getDescription() {
        return this.food.getDescription()+", adding chicken meat";//TODO Implement
    }

    @Override
    public double cost() {
        return this.food.cost()+4.5;//TODO Implement
    }
}
