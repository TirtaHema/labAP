package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food; //TODO Implement
    }

    @Override
    public String getDescription() {
        return this.food.getDescription()+", adding cheese";//TODO Implement
    }

    @Override
    public double cost() {
        return this.food.cost()+2.0;//TODO Implement
    }
}
