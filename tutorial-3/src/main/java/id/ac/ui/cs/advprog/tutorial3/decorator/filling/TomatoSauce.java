package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;

    public TomatoSauce(Food food) {
        this.food = food; //TODO Implement
    }

    @Override
    public String getDescription() {
        return this.food.getDescription()+", adding tomato sauce";//TODO Implement
    }

    @Override
    public double cost() {
        return this.food.cost()+0.2;//TODO Implement
    }
}
